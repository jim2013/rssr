/* An xml parser which provides Simple API for xml/html(SAX)
 * It's for my own side projects, so merely support the minimal rules of xml, 
 * as follows:
 * 1. root = (processing-instruction element1 | element2 | comment | script)*
 * 2. processing-instrction = <?EN [.\n]* ?>
 * 3. element1 = <element-name attribute* whitespace* > (content | (element1 | element2)*) </element-name>
 * 4. element2 = <element-name attribute* whitespace* />
 * 5. element-name = [^whitespace]+
 * 6. content = [^<]* | CDATA
 * 7. attribute = whitespace* AttrName="AttrValue" whitespace*
 * 8. comment =  <!-- [.\n]* -->
 *    [.\n]* this is a regular expression that represent any characters
 * 9. CDATA =  <![DATA[ [.\n]* ]]>
 * 10. script = <script> [.\n]* </script>
 *    In order to allow `<` and `>` in html script element content, treat
 *    everythings between <script> and </script> as content, it's simple but 
 *    features-limited which means, for example, no child elements are 
 *    supported under script element.
 *    For more see: https://html.spec.whatwg.org/multipage/scripting.html#restrictions-for-contents-of-script-elements
 *
 * # Name convention
 * ElemName: element name
 * attrName: attribute name
 * attrValue: attribute value
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include "xml.h"

#define DEBUG 0
#define EN_MAX 32
#define DATA_MAX 1024
#define ATTRS_MAX 8
#define ATTR_NAME_MAX 32
#define ATTR_VALUE_MAX 512
#define NESTED_LEVEL_MAX 8
#define LAST_CHARS_MAX 10
#define UNPROCESSED_MAX 16
#define IS_WHITESPACE(ch) (ch==' '||ch=='\t'||ch=='\r'||ch=='\n')

#define STATUS_CONTENT 0
#define STATUS_ELEM_NAME 1
#define STATUS_ATTR_NAME 2
#define STATUS_ATTR_VALUE 3
#define STATUS_CDATA 4
#define STATUS_COMMENT 5
/* when processing html, the script element may contain special 
 * characters '<', '>' we should treat these special character as content.
 *
 * For more see: https://html.spec.whatwg.org/multipage/scripting.html#restrictions-for-contents-of-script-elements
 */
#define STATUS_SCRIPT 6

/**TODO validate **/

struct xmlContext *newXMLContext(void *privateData)
{
	struct xmlContext *ctx = malloc(sizeof(struct xmlContext));
	ctx->status = STATUS_CONTENT;
	ctx->lastChars = malloc(LAST_CHARS_MAX * sizeof(char));
	ctx->lastChars[0] = '\0';
	ctx->lastCharSize = 0;
	ctx->attrs = malloc(ATTRS_MAX * sizeof(struct xmlAttr*));
	for (int i = 0; i < ATTRS_MAX; i++)
		ctx->attrs[i] = NULL;
	ctx->attrSize = 0;
	ctx->attrNameOrValueSize = 0;
	ctx->data = malloc(DATA_MAX * sizeof(char));
	ctx->dataSize = 0;
	ctx->privateData = privateData;
	return ctx;
}

struct xmlAttr *newXMLAttr()
{
	struct xmlAttr *attr = malloc(sizeof(struct xmlAttr));
	attr->name = malloc(ATTR_NAME_MAX * sizeof(char));
	attr->name[0] = '\0';
	attr->value = malloc(ATTR_VALUE_MAX * sizeof(char));
	attr->value[0] = '\0';
	return attr;
}

void freeXMLAttr(struct xmlAttr *attr)
{
	if (attr == NULL)  return;
	if (attr->name != NULL)  free(attr->name);
	if (attr->value != NULL)  free(attr->value);
	free(attr);
	return;
}

/* free ctx->privateData before calling this function */
void freeXMLContext(struct xmlContext *ctx)
{
	if (ctx == NULL)  return;
	free(ctx->lastChars);
	for (int i = 0; i < ATTRS_MAX; i++)
		freeXMLAttr(ctx->attrs[i]);
	free(ctx->attrs);
	free(ctx->data);
	free(ctx);
}

void contentCallBackAndResetBuffer(struct xmlContext *ctx, struct xmlCallBack *cb)
{
	if (DEBUG)  printf("content cb. dataSize=%d\n", ctx->dataSize);
	if (cb == NULL || cb->content == NULL || ctx->dataSize < 1) 
	{
		if (DEBUG)  printf("do nothing. return\n");
		/* reset buffer*/
		ctx->dataSize = 0;
		return;
	}

	/* make sure data is \0 terminated string*/
	ctx->data[ctx->dataSize] = '\0';
	cb->content(ctx->data, ctx->dataSize, ctx->privateData);
	/* reset buffer*/
	ctx->dataSize = 0;
}

void beginOrEndCallBack(struct xmlContext *ctx, struct xmlCallBack *cb)
{
	char *elemName = ctx->data;
	bool begin = true;
	bool end = false;
	if (elemName[0] == '/')
	{
		/* </abc> */
		++elemName;
		begin = false;
		end = true;
	} else if (elemName[ctx->dataSize-1] == '/') {
		/* <abc/> */
		elemName[ctx->dataSize - 1] = '\0';
		end = true;
	} else if (ctx->attrSize > 0) {
		/* <abc a1="1" /> */
		char *attrName = ctx->attrs[ctx->attrSize-1]->name;
		if (ctx->attrs[ctx->attrSize-1]->value[0] == '\0' &&
				attrName != NULL && attrName[0] == '/' && attrName[1] == '\0')
		{
			end = true;
			ctx->attrSize--;
			freeXMLAttr(ctx->attrs[ctx->attrSize]);
			ctx->attrs[ctx->attrSize] = NULL;
		}
	}

	ctx->status = strcmp("script", elemName) || end ? STATUS_CONTENT : STATUS_SCRIPT;

	if (begin && cb && cb->begin)
		cb->begin(elemName, ctx->attrs, ctx->attrSize, ctx->privateData);
	if (end && cb && cb->end)
		cb->end(elemName, ctx->privateData);

	ctx->lastCharSize = 0;
	for (int i = 0; i < ctx->attrSize; i++)
	{
		freeXMLAttr(ctx->attrs[i]);
		ctx->attrs[i] = NULL;
	}
	ctx->attrSize = 0;
	ctx->attrNameOrValueSize = 0;
	ctx->dataSize = 0;
}

int parseXML(char *data, int size, struct xmlContext *ctx,
	   	struct xmlCallBack *cb)
{
	if (DEBUG)  printf("begin to parse.\n");
	if (data == NULL)
	{
		printf("input data is empty!");
	  	return 1;
	}
	int i = 0; /* data index */
	int j = -1;  /* unprocessed index */
	/*
	 * */
	char * unprocessed = malloc(UNPROCESSED_MAX * sizeof(char));
	while(i < size || j > -1)
	{
		char ch = '\0';
		if (j > -1)
			ch = unprocessed[j++];
		else
			ch = data[i++];
		if (DEBUG)
			printf("%c i=%d %d\n", ch, i, ctx->status);

		if (ctx->status == STATUS_CONTENT || ctx->status == STATUS_CDATA)
		{
			char *terminal = ctx->status == STATUS_CONTENT ? "<!{CDATA[" : "]]>";
			if (terminal[ctx->lastCharSize] == ch)
			{
				if (ctx->lastCharSize >= LAST_CHARS_MAX)
				{
					printf("last chars exceed max.\n");
					goto err;
				}
				/* at least match the first n terminal charaters */
				ctx->lastChars[ctx->lastCharSize++] = ch;
				if (terminal[ctx->lastCharSize] == '\0')
				{
					ctx->status = ctx->status == STATUS_CONTENT ? STATUS_CDATA
						: STATUS_CONTENT;	 /* match the terminal */
					ctx->lastCharSize = 0;
				}
			} else if (ctx->lastCharSize) {
				if (ctx->lastCharSize >= UNPROCESSED_MAX)
				{
					printf("unprocessed exceed max.\n");
					goto err;
				}
				ctx->lastChars[ctx->lastCharSize++] = ch;
				if (j > -1 && unprocessed[j] != '\0')
				{
					printf("unexcepted situation, nested unprocessed need to be processed .\n");
					goto err;
				}

				/* consume first charater in ctx->lastChars */
				if (ctx->status == STATUS_CONTENT)
				{

					/* first character is < and not CDATA, next status must 
					 * be ELEM_NAME. Before go to next status, we need to CallBack 
					 * content function*/
					contentCallBackAndResetBuffer(ctx, cb);
					ctx->status = STATUS_ELEM_NAME;
					ctx->attrSize = 0;
					ctx->dataSize = 0;
				} else {
					/* not the end of CDATA, consume the first character as content */
					ctx->data[ctx->dataSize++] = ctx->lastChars[0];
					if (ctx->dataSize == DATA_MAX - 1)
						contentCallBackAndResetBuffer(ctx, cb);
				}

				/* put the remaining characters into unprocessed */
				strncpy(unprocessed, ctx->lastChars + 1, ctx->lastCharSize - 1);
				unprocessed[ctx->lastCharSize - 1] = '\0';
				j = 0;
				ctx->lastCharSize = 0;
			} else {
				/* normal content */
				ctx->data[ctx->dataSize++] = ch;
				if (ctx->dataSize >= DATA_MAX - 1)
					/* data buffer is full, call content CallBack*/
					contentCallBackAndResetBuffer(ctx, cb);
			}
		} else if (ctx->status == STATUS_ELEM_NAME) {
			if (ch == '>')
			{
				if (ctx->dataSize < 1 || 
						(ctx->dataSize == 1 && ctx->data[0] == '/'))
				{
					printf("malformed, element name is empty.\n");
					goto err;
				}
				ctx->data[ctx->dataSize] = '\0';
				beginOrEndCallBack(ctx, cb);
			} else if (IS_WHITESPACE(ch)) {
				if (ctx->dataSize == 0)
				{
					printf("malformed, element name is empty.\n");
					goto err;
				}
				ctx->data[ctx->dataSize] = '\0';
				if (!strcmp("!--", ctx->data)) 
				{
					ctx->status = STATUS_COMMENT;
					ctx->lastCharSize = 0;
				} else {
					ctx->status = STATUS_ATTR_NAME;
					ctx->lastCharSize = 0;
					ctx->attrSize = 0;
					ctx->attrNameOrValueSize = 0;
				}
			} else {
				ctx->data[ctx->dataSize++] = ch;
			}
		} else if (ctx->status == STATUS_ATTR_NAME) {
			if (ctx->attrNameOrValueSize >= ATTR_NAME_MAX)
			{
				printf("exceed max attrName size.\n");
				goto err;
			}

			char *attrName = NULL;
			if (ctx->attrNameOrValueSize)
				attrName = ctx->attrs[ctx->attrSize-1]->name;
			if (IS_WHITESPACE(ch))
			{
				/* skip white space */
			} else if (ch == '=') {
				if (ctx->attrNameOrValueSize == 0)
				{
					printf("malformed, attrName can't be empty.\n");
					goto err;
				}
				attrName[ctx->attrNameOrValueSize] = '\0';

				ctx->status = STATUS_ATTR_VALUE;
				ctx->attrNameOrValueSize = -1;
			} else if (ch == '>') {
				if (attrName)  attrName[ctx->attrNameOrValueSize] = '\0';
				beginOrEndCallBack(ctx, cb);
			} else {
				if (ctx->attrNameOrValueSize && ctx->lastCharSize 
						&& IS_WHITESPACE(ctx->lastChars[0]))
				{
					/* Last attribute has no value, that's ok, we embark 
					 * upon a new attribute*/
					attrName[ctx->attrNameOrValueSize] = '\0';
					ctx->attrNameOrValueSize = 0;
				}

				if (ctx->attrNameOrValueSize == 0) 
				{
					if (ctx->attrSize >= ATTRS_MAX)
					{
						printf("exceed max attrSize.\n");
						goto err;
					}
					ctx->attrs[ctx->attrSize++] = newXMLAttr();
					attrName = ctx->attrs[ctx->attrSize - 1]->name;
				}
				attrName[ctx->attrNameOrValueSize++] = ch;
			}

			if (ctx->status == STATUS_ATTR_NAME) 
			{
				/* buffer last character, we need it to distinguish between a no 
				 * value attribute and a attribute with white spaces around =, 
				 * for example
				 * 1. <tree name = "fish" />
				 * 2. <tree name age="17" />
				 * */
				ctx->lastChars[0] = ch;
				ctx->lastCharSize = 1;
			}
		} else if (ctx->status == STATUS_ATTR_VALUE) {
			char *attrValue = ctx->attrs[ctx->attrSize - 1]->value;
			if (ctx->attrNameOrValueSize == -1)
			{
				/* 1. whitespace:ignore whitespace before double quote(")
				 * 2. double quote("): set ctx->attrNameOrValueSize from -1 to 0,
				 *    it indicates the begining of attribute value
				 * 3. other charaters 
				 * indicate empty attribute value and a new attribute 
				 * begins, we take a step back, the character should be 
				 * processed in the ATTR_NAME case */
				if (ch == '"') 
				{
					ctx->attrNameOrValueSize = 0;
				} else if (!IS_WHITESPACE(ch)) {
					if (j > -1)  j--;
					else  i--;
					ctx->status = STATUS_ATTR_NAME;
					ctx->attrNameOrValueSize = 0;
					ctx->lastCharSize = 0;
				}
			} else {
				if (ctx->attrNameOrValueSize >= ATTR_VALUE_MAX)
				{
					printf("exceed max attrValue size.\n");
					goto err;
				}
				if (ch == '"')
				{
					attrValue[ctx->attrNameOrValueSize] = '\0';
					ctx->status = STATUS_ATTR_NAME;
					ctx->attrNameOrValueSize = 0;
					ctx->lastCharSize = 0;
				} else {
					attrValue[ctx->attrNameOrValueSize++] = ch;
				}
			}
		} else if (ctx->status == STATUS_COMMENT) {
			/* terminal condition: --> */
			char *terminal = "-->";
			if (terminal[ctx->lastCharSize] == ch)
			{
				ctx->lastCharSize++;
				if (ctx->lastCharSize == 3)
				{
					ctx->status = STATUS_CONTENT;
					ctx->lastCharSize = 0;
					ctx->dataSize = 0;
				}
			} else if (ctx->lastCharSize) {
				if (ch == '-')
					/* more then two consective - */
					ctx->lastCharSize = 2;
				else
					ctx->lastCharSize = 0;
			}
		} else if (ctx->status == STATUS_SCRIPT) {
			/* terminal condition: </script> 
			 * ignore everythings between script */
			char *terminal = "</script>";
			if (terminal[ctx->lastCharSize] == ch)
			{
				ctx->lastCharSize++;
				if (ctx->lastCharSize == 9)
				{
					if (UNPROCESSED_MAX < 9)
					{
						printf("exceeds unprocessed max.\n");
						goto err;
					}
					ctx->status = STATUS_ELEM_NAME;
					strncpy(unprocessed, terminal + 1, 9);
					unprocessed[9] = '\0';
					j = 0;
					ctx->lastCharSize = 0;
					ctx->dataSize = 0;
					ctx->attrSize = 0;
				}
			} else if (ctx->lastCharSize) {
				ctx->lastCharSize = 0;
			}
		} else {
			printf("unknown parse xml status %d\n", ctx->status);
			goto err;
		}

		if (j > -1 && unprocessed[j] == '\0')
			j = -1;
	}

	free(unprocessed);
	return 0;

err:
	printf("error occred near:");
	for (int k = (i - 10 > 0 ? i -10 : 0); k < i + 10 && k < size; k++)
	{
		printf("%c", data[k]);
	}
	free(unprocessed);
	return 1;
}
