#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <fcntl.h>
#include <errno.h>
#include <string.h>
#include <sys/stat.h>

#define debug 0

#define C_OK 0
#define C_ERR 1
#define C_CONTINUE 2

#define UNUSED(x) (void)(x)

#define URL_LENGTH_MAX 1024
#define CRON_MAX 64
#define RSS_FN_MAX 32
#define DELIMITER ';'
struct RSSSource 
{
	char *cron;
	char *url;
	char *filename;
};

enum ParseStatus {NEW_LINE, COMMENT, CRON, URL, FILENAME};

struct RSSSource* newRSSSource()
{
	struct RSSSource *rssSource = malloc(sizeof(struct RSSSource));
	rssSource->cron = malloc(CRON_MAX * sizeof(char));
	rssSource->url = malloc(URL_LENGTH_MAX * sizeof(char));
	rssSource->filename = malloc(RSS_FN_MAX * sizeof(char));
	return rssSource;
}

void freeRSSSource(struct RSSSource *rssSource)
{
	if (rssSource == NULL)  return;
	if (rssSource->filename)  free(rssSource->filename);
	if (rssSource->url)  free(rssSource->url);
	if (rssSource->cron) free(rssSource->cron);
	free(rssSource);
}

int syncWrite(int fd, char *str, int size)
{
	int wSize = write(fd, str, size);
	if (size == -1) 
	{
		printf("fail to write to file: %s\n", strerror(errno));
		return C_ERR;
	} else if (wSize < size) {
		printf("fail to write all data to file: %s\n",
			   	strerror(errno));
		return C_ERR;
	}
	return C_OK;
}

int writeCronComment(int fd)
{
	char *comment = 
	  "# to install these cron jobs, execute bash command: crontab xxx(replace with this filename) \n"
	  "# m(0-59) h(0-23)  dom(day of month:1-31) mon(month of year: 1-12) dow(day of week: 0-6)   command\n"
	  "# */1: repeat every 1 minute\n"
	  "# 1,31: repeat 1 and 31 minute\n"
	  "# * * * * * ${HOME}/workspace/rssr/fetch.sh feedURL ${HOME}/www/rss/feedName.xml\n";
	return syncWrite(fd, comment, strlen(comment));
}

int writeCronTask(int fd, struct RSSSource *source)
{
	int ret = 0;
	ret = syncWrite(fd, source->cron, strlen(source->cron));
	if (ret != C_OK)  return ret;

	char *fetchCmd = " ${HOME}/workspace/rssr/fetch.sh ";
	ret = syncWrite(fd, fetchCmd, strlen(fetchCmd));
	if (ret != C_OK)  return ret;

	ret = syncWrite(fd, source->url, strlen(source->url));
	if (ret != C_OK)  return ret;
	
	char *format = " ${HOME}/www/rss/%s\n";
	int size = 19 + strlen(source->filename);
	char fullName[size]; 
	snprintf(fullName, size, format, source->filename);
	return syncWrite(fd, fullName, strlen(fullName));
}

int writeOPMLStart(int fd)
{
	char *start = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
	"<opml version=\"2.0\">\n"
    "	<body>\n";
	return syncWrite(fd, start, strlen(start));
}

int writeOPMLOutline(int fd, struct RSSSource *source)
{
    char *format = "		<outline xmlUrl=\"http://songziyu.cc/rss/%s\" />\n";
	int size = 48 + strlen(source->filename);
	char outline[size];
	snprintf(outline, size, format, source->filename);
	return syncWrite(fd, outline, size - 1);
}

int writeOPMLEnd(int fd)
{
	char *end = "	</body>\n</opml>\n";
	return syncWrite(fd, end, strlen(end));
}

int parseSource(char *buf, int bufSize, struct RSSSource *source, 
		enum ParseStatus *parseStatus, int *lineNum, int *consumeredSize)
{
	int i = 0, j = 0;
	char ch = '\0';
	while(i < bufSize)
	{
		ch = buf[i++];
		if (debug) printf("ch is %c, parseStatus is %d\n", ch, *parseStatus);
		if (*parseStatus == NEW_LINE) {
			/* NEW_LINE status means a new line begins, but the next 
			 * parseStatus maybe COMMENT or CRON which is decided by the first 
			 * character of the newline.
			 *
			 * If the next parseStatus is CRON, we merely modify the 
			 * parseStatus but don't process the character here, we take a step
			 * back and the character will be processed at CRON case in the 
			 * next loop
			 */
			if (ch == '\n' || ch == '\r')
			{
				/* empty line or different format new line feed, still NEW_LINE
				 * status */
			} else if (ch == '#') {
				/* comments start with # chracter*/
				*parseStatus = COMMENT;
			} else {
				*parseStatus = CRON;
				j = 0;
				/* take a step back, the character will be processed at CRON 
				 * case in the next loop */
				--i;
			}
		} else if (*parseStatus == COMMENT) {
			/* skip comment until new line */
			if (ch == '\r' || ch == '\n') *parseStatus = NEW_LINE;
		} else if (*parseStatus == CRON || *parseStatus == URL) {
			int max = 0;
			char *field = NULL;
			enum ParseStatus nextStatus = URL;
			if (*parseStatus == CRON)
			{
				max = CRON_MAX;
				field = source->cron;
				nextStatus = URL;
			} else if (*parseStatus == URL){
				max = URL_LENGTH_MAX;
				field = source->url;
				nextStatus = FILENAME;
			}
			if (j >= max)
			{
				printf("cron exceeds max length %d, at line %d\n", 
						CRON_MAX - 1, *lineNum);
				return C_ERR;
			}
			if (ch == '\r' || ch == '\n')
			{
				printf("bad format at line %d\n", *lineNum);
				return C_ERR;
			} else if (ch == DELIMITER) {
				/* make sure cron is \0 terminated*/
				field[j] = '\0';
				*parseStatus = nextStatus;
				j = 0;
			} else {
				field[j++] = ch;
			}
		}  else if (*parseStatus == FILENAME) {
			if (j >= RSS_FN_MAX)
			{
				printf("filename exceeds max length %d, at line %d",
						RSS_FN_MAX - 1, *lineNum);
				return C_ERR;
			}
			if (ch == DELIMITER) 
			{
				printf("bad format, should not have more fields after filename," 
						"at line %d", *lineNum);
			} else if (ch == '\r') {
				/* skip */
			} else if (ch == '\n') {
				/* make sure filename is \0 terminated */
				source->filename[j] = '\0';
				*parseStatus = NEW_LINE;
				*consumeredSize += i;
				++*lineNum;
				return C_OK;
			} else {
				source->filename[j++] = ch;
			}
		}

		/* support \n or \r\n as new line feed */
		if (ch == '\n')  ++*lineNum;
	}

	*consumeredSize += i;
	return C_CONTINUE;
}

int main(int argc, char *argv[])
{
	/*avoid gcc unsed warnings*/
	UNUSED(argc);
	UNUSED(argv);

	int bufSize = 1024;
	char buf[bufSize];
	struct RSSSource *source = NULL;
	char *fn_source = "source.txt";
	int fd_source = -1, fd_cron = -1, fd_opml = -1;
	fd_source = open(fn_source, O_RDONLY);
	if (fd_source == -1)
	{
		printf("fail to open %s, error msg is: %s\n", fn_source, strerror(errno));
		return 1;
	}

	int mode = S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH;

	char *fn_cron = "cron.txt";
	fd_cron = open(fn_cron, O_WRONLY|O_CREAT, mode);
	if (fd_cron == -1)
	{
		printf("fail to open or create %s,  error msg is: %s\n", fn_cron,
				strerror(errno));
		goto err;
	}
	if (writeCronComment(fd_cron) != C_OK)
	{
		printf("fail to write cron comment to %s\n", fn_cron);
		goto err;
	}

	char *fn_opml = "myrss.opml";
	fd_opml = open(fn_opml, O_WRONLY|O_CREAT, mode);
	if (fd_opml == 1)
	{
		printf("fail to open or create %s, error msg is: %s \n", fn_opml,
				strerror(errno));
		goto err;
	}
	if (writeOPMLStart(fd_opml) != C_OK)
	{
		printf("fail to write opml start to file %s : %s\n", fn_opml,
				strerror(errno));
		goto err;
	}
	
	source = newRSSSource();
	enum ParseStatus parseStatus = NEW_LINE;
	int lineNum = 1;

	ssize_t rSize = 0;
	while((rSize = read(fd_source, buf, bufSize)) > 0) 
	{
		int parseRet = 0;
		int consumeredSize = 0;
		do {
			parseRet = parseSource(buf + consumeredSize, rSize - consumeredSize,
					source, &parseStatus, &lineNum, &consumeredSize);

			if (parseRet == C_ERR)	goto err;
			if (parseRet == C_OK)
			{
				if (writeCronTask(fd_cron, source) != C_OK) goto err;
				if (writeOPMLOutline(fd_opml, source) != C_OK) goto err;
			} else if (parseRet == C_CONTINUE) {
				break;
			} else {
				printf("unkonwn error code\n");
				goto err;
			}
		} while(consumeredSize < rSize);

		if (rSize < bufSize) break;
	}

	if (rSize == -1)
	{
		printf("fail to read from source file %s : %s", fn_source, 
				strerror(errno));
		goto err;
	}
	writeOPMLEnd(fd_opml);
	
	close(fd_source);
	close(fd_cron);
	close(fd_opml);
	freeRSSSource(source);
	return 0;

err:
	if (fd_source != -1) close(fd_source);
	if (fd_cron != -1) close(fd_cron);
	if (fd_opml != -1) close(fd_opml);
	if (source != NULL) freeRSSSource(source);
	return 1;
}
