#include <stdio.h>
#include <unistd.h>
#include <stdbool.h>
#include <fcntl.h>
#include <string.h>
#include <stdlib.h>
#include "xml.h"
#include "json.h"

#define DEBUG 0
#define UNUSED(x) (void)(x)

struct bttt_parseContext
{
	bool inBotDiv;
	bool inAElement;
};

void 
bttt_begin(char *en, struct xmlAttr *attrs[], int attrSize, void *privateData)
{
	if (DEBUG)  printf("begin callback: <%s>\n", en);

	struct bttt_parseContext *ctx = 
		(struct bttt_parseContext *)privateData;
	char * class = NULL;
	for (int i = 0; i < attrSize; i++)
	{
		if (!strcmp(attrs[i]->name, "class"))
			class = attrs[i]->value;
	}
	if (!strcmp(en, "div") && class && !strcmp(class, "bot"))
	{
		ctx->inBotDiv = true;
	} else if (!strcmp(en, "a") && ctx->inBotDiv) {
		ctx->inAElement = true;
		for (int i = 0; i < attrSize; i++)
		{
			if (!strcmp(attrs[i]->name, "href"))
				printf("link is %s  ", attrs[i]->value);
		}
	}
}

void 
bttt_end(char *en, void *privateData) 
{
	struct bttt_parseContext *ctx = 
		(struct bttt_parseContext *)privateData;
	if (!strcmp(en, "a") && ctx->inAElement) ctx->inAElement = false;
	else if (!strcmp(en, "div") && ctx->inBotDiv) ctx->inBotDiv = false;
}

void 
bttt_content(char *data, int size, void *privateData)
{
	UNUSED(size);
	struct bttt_parseContext *ctx = 
		(struct bttt_parseContext *)privateData;
	if (ctx->inBotDiv && ctx->inAElement) printf("name is %s\n", data);
}

int testXML()
{
	int fd = open("tcjbf.html", O_RDONLY);
	if (fd == -1)
	{
		printf("fail to open.\n");
		return 1;
	}
	struct bttt_parseContext bttt_ctx = {
		.inBotDiv = false,
		.inAElement = false
	};
	if (DEBUG)
		printf("before new xml context.\n");
	struct xmlContext *ctx = newXMLContext(&bttt_ctx);
	if (DEBUG)
		printf("after new xml context.\n");
	struct xmlCallBack cb;
	cb.begin = bttt_begin;
	cb.end = bttt_end;
	cb.content = bttt_content;

	char *buf = malloc(1024 * sizeof(char));
	buf[0] = '\0';
	int nread = 0;
	while ((nread = read(fd, buf, 1024)) > 0 && !parseXML(buf, nread, ctx, &cb));
	freeXMLContext(ctx);
	free(buf);
	close(fd);
	return 0;
}

void jsonFormatBegin(char *name, int valType, int level, void *clientData)
{
	int *lastLevel = (int *)clientData;
	if (level < 0)
	{
		printf("error: level < 0.\n");
		return;
	}

	if (level == *lastLevel && level != 0)
		printf(",\n");
	for (int i = 0; i < level; i++)
		printf("\t");
	if (name && name[0] != '\0')
	{
		printf("\"%s\" : ", name);
	}
	if (valType == VAL_OBJ)
		printf("{\n");
	else if (valType == VAL_ARR)
		printf("[\n");
	else
		printf("UNKNOWN");

	*lastLevel = level;
}

void jsonFormatEnd(char *name, int valType, int level, void *clientData)
{
	UNUSED(clientData);
	UNUSED(name);
	if (level < 0)
	{
		printf("error: level < 0.\n");
		return;
	}

	printf("\n");
	for (int i = 0; i < level; i++)
		printf("\t");
	if (valType == VAL_OBJ)
		printf("}");
	else if (valType == VAL_ARR)
		printf("]");
	else
		printf("UNKNOWN");

	int *lastLevel = (int *)clientData;
	*lastLevel = level;
}

void jsonFormatKV(char *name, char *val, int valType, int level, void *clientData)
{
	int *lastLevel = (int *)clientData;
	if (level < 0)
	{
		printf("error: level < 0.\n");
		return;
	}

	if (level == *lastLevel)
	{
		printf(",\n");
	}
	for (int i = 0; i < level; i++)
		printf("\t");

	if (name && name[0] != '\0')
		printf("\"%s\" : ", name);
	if (valType == VAL_STR)
		printf("\"%s\"", val);
	else
		printf("%s", val);
	
	*lastLevel = level;
}

/* test json parser */
void testJSON()
{
	char *data = "{\"obj\":{\"i\":123, \"d\":1.23, \"exp\":1.2E6}, \"str\":\"hello json!\", \"b\":true, \"arr\":[1, 2, 3]}";
	int lastLevel = 0;
	struct jsonContext *ctx = newJSONContext(&lastLevel);
	struct jsonCallBack cb = 
		{
			.begin = jsonFormatBegin,
			.end = jsonFormatEnd,
			.kv = jsonFormatKV
		};
	parseJSON(data, strlen(data), ctx, &cb);
	freeJSONContext(ctx);
}

int main(int argc, char *argv[])
{
	UNUSED(argc);
	UNUSED(argv);
	testJSON();
}
