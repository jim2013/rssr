#!/bin/sh

# fetch the latest rss feed with curl
if [ $# != 2 ]; then
	echo 'usage: fetch.sh feedURL filename'
	exit 1
fi
tmp=$2".tmp"
curl -k $1 > $tmp
mv $tmp $2
