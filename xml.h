#ifndef __XML_H__
#define __XML_H__

struct xmlAttr
{
	char *name;
	char *value;
};

struct xmlContext
{
	int status;

	/* buffered characters */
	char *lastChars;
	int lastCharSize;

	/* attribute */
	struct xmlAttr **attrs;
	int attrSize;
	int attrNameOrValueSize;

	/*content or element name */
	char *data;
	int dataSize;

	void *privateData;
};

struct xmlCallBack
{
	void (*begin)(char *en, struct xmlAttr *attrs[], int attrSize, void *privateData);
	void (*end)(char *en, void *privateData);
	void (*content)(char *data, int size, void *privateData);
};

struct xmlContext *newXMLContext(void *privateData);
void freeXMLContext(struct xmlContext *ctx);

int parseXML(char *data, int size, struct xmlContext *ctx,
	   	struct xmlCallBack *cb);

#endif
