#!/bin/bash

if [[ $# != 1 ]]; then
	echo get youtube channel rss url, useage: yt_id youtube_channel_url
	exit
fi

rss=$(curl -x socks5://127.0.0.1 "$1" | sed -nE 's/.*href="(https:\/\/www.youtube.com\/feeds\/videos.xml\?channel_id=[^"]*)".*/\1/p')
echo $rss
