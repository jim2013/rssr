/**
 * # motivation
 * so many rss feeds are blocked in china, we need get these blocked feeds by a 
 * rss relay service
 *
 * # what
 * a. fetch feeds at a remote server periodically
 * b. provide rss feed
 *
 * # fetch rss feed
 * rss feed is fetched by http, rssr use curl library to fetch it and plan to
 * replace curl with its own http client implementation in the future
 *
 * # rss feed provider
 * it's actually a http server, now rssr use althttp server and plan to 
 * implement it in the future
 *
 * 
 * # latest feed storage
 * save the exact copy of the original rss feed in the file
 *
 * # history feed storage
 * 1. every rss feed is in a folder, folder name is feed name
 * 2. channel info of feed is stored inside feed folder, file name is 
 * channel.xml
 * 3. every item is stored in a signal file,file name is increasing positive 
 * integer
 * 4. there is a special file record latest 500 items, include item's file name
 * , item's title and pubDate(optional), file name is recent.txt
 *	
 * # complie
 * cc -lcurl rssr.c
 *
 * # Copyright
 * 2021 jim
 *
 * # License
 * GPL
 * 
 */

#include <errno.h>
#include <fcntl.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

#include <curl/curl.h>

#define UNUSED(x) (void)(x)

struct MemoryStruct {
  char *memory;
  size_t size;
};

static size_t
writeMemoryCallback(void *contents, size_t size, size_t nmemb, void *userp)
{
  size_t realsize = size * nmemb;
  struct MemoryStruct *mem = (struct MemoryStruct *)userp;

  char *ptr = realloc(mem->memory, mem->size + realsize + 1);
  if(!ptr) {
    /* out of memory! */
    printf("not enough memory (realloc returned NULL)\n");
    return 0;
  }

  mem->memory = ptr;
  memcpy(&(mem->memory[mem->size]), contents, realsize);
  mem->size += realsize;
  mem->memory[mem->size] = 0;

  return realsize;
}

int
getFeed(char * url, struct MemoryStruct *resp)
{
	int ret = 0;
	CURL *curl = NULL;
	CURLcode res;
	/* init the curl session */
	curl = curl_easy_init();
	if(curl) {
		/* specify url to get, it's required */
		curl_easy_setopt(curl, CURLOPT_URL, url);

		/* send all received data to this function */
		curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, writeMemoryCallback);

		/* we pass our resp buffer to the callback function */
		curl_easy_setopt(curl, CURLOPT_WRITEDATA, (void *)resp);

		/* Perform the request, res will get the return code */
		res = curl_easy_perform(curl);
		/* Check for errors */
		if(res != CURLE_OK)
		{
		  fprintf(stderr, "curl_easy_perform() failed: %s\n",
				  curl_easy_strerror(res));
		  ret = 1;
		}

		/* always cleanup */
		curl_easy_cleanup(curl);
	} else {
		ret = 1;
	}
	return ret;
}


#define EN_MAX 32
#define ATTR_MAX 256
#define DATA_MAX 1024
enum xmlStage {INIT, OPEN_TAG, CLOSE_TAG};
struct xmlContext
{
	enum xmlStage stage;

	/* attribute data */
	char attr[ATTR_MAX];
	int attrSize;

	/*content data or element name data*/
	char data[DATA_MAX];
	int dataSize;

};

struct xmlCallBack
{
	void (*begin)(char *en, short int enSize, char *attr, int attrSize) ;
	void (*end)(char *en, short int enSize);
	void (*content)(char *data, int size);
};

int
parseXML(char *data, size_t size, struct xmlContext *ctx, struct xmlCallBack *cb)
{
	size_t i = 0;
	while(i < size)
	{
		char ch = *(data + i);

		if (ch == '<')
		{
			/* last content buffer call back*/
			if (ctx->dataSize > 0)
			{
				cb->content(ctx->data, ctx->dataSize);
				ctx->dataSize = 0;
				memset(ctx->data, '\0', DATA_MAX);
			} 
			ctx->stage = OPEN_TAG;
		} else if (ch == '>') {
			if (ctx->dataSize == 0) 
				goto malformed;
			
			if (ctx->data[0] == '/')
			{
				/* element end event call back*/
				cb->end(ctx->data + 1, ctx->dataSize);
				ctx->dataSize = 0;
				memset(ctx->data, '\0', DATA_MAX);
			} else {
				/* element begin event call back*/
				if (ctx->attrSize == 0) 
				{
					/* no attribute */
					cb->begin(ctx->data, ctx->dataSize, ctx->attr, ctx->attrSize);
				} else {
					int attrSize = ctx->attrSize;
					if (ctx->attr[attrSize - 1] == '/')
						attrSize = attrSize - 1;
					cb->begin(ctx->data, ctx->dataSize, ctx->attr, attrSize);
					ctx->attrSize = 0;
					memset(ctx->attr, '\0', ATTR_MAX);
				}
				ctx->dataSize = 0;
				memset(ctx->data, '\0', DATA_MAX);
			}
			ctx->stage = CLOSE_TAG;
		} else {
			if (ctx->stage == OPEN_TAG)
			{
				/* content between tag*/
				if (ctx->attrSize > 0)
				{
					/* attribute */
					if (ctx->attrSize > ATTR_MAX) goto exceedLimit;
					ctx->attr[ctx->attrSize++] = ch;
				} else {
					/* element name*/
					if (ch == ' ')
					{
						if (ctx->dataSize == 0) goto malformed;

						ctx->attr[ctx->attrSize++] = ch;
					} else {
						if (ctx->dataSize >= DATA_MAX) goto exceedLimit;

						ctx->data[ctx->dataSize++] = ch;
					}
				}
			} else { 
				/* content outside tag*/
				if (ctx->dataSize >= DATA_MAX) 
				{
					/* content call back*/
					cb->content(ctx->data, ctx->dataSize);
					ctx->dataSize = 0;
					memset(ctx->data, '\0', DATA_MAX);
				}
				ctx->data[ctx->dataSize++] = ch;
			} 
		}

		i++;
	}
	return 0;

malformed:
	printf("malformed, near:");
	for (size_t j = i; j < 10 && j < size; j++)
	{
		printf("%c", *(data + j));
	}
	return 1;

exceedLimit:
	printf("exceedLimit, near:");
	for (size_t j = i; j < 10 && j < size; j++)
	{
		printf("%c", *(data + j));
	}
	return 1;
}

void 
begin(char *en, short int enSize, char *attr, int attrSize)
{
	UNUSED(enSize);
	UNUSED(attrSize);
	printf("<%s%s>", en, attr);
}

void 
end(char *en, short int enSize) 
{
	UNUSED(enSize);
	printf("</%s>", en);
}

void 
content(char *data, int size)
{
	UNUSED(size);
	printf("%s", data);
}

int
main(int argc, char *argv[])
{
	/*avoid gcc unsed warnings*/
	UNUSED(argc);
	UNUSED(argv);

	char *feedURL = "https://www.rssboard.org/files/sample-rss-2.xml";
	struct MemoryStruct *resp = malloc(sizeof(struct MemoryStruct));
	resp->memory = malloc(1);
	resp->size = 0;
	int fd = -1;
	if (getFeed(feedURL, resp) == 0) 
	{
		/* store latest original feed */
		fd = open("scmp-feed.xml.tmp", O_WRONLY | O_CREAT, 
				S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH);
		if (fd == -1)
		{
			printf("fail to open:%s", strerror(errno));
		} else {
			size_t i = 0;
			size_t size = 1024;
			while (i < resp->size)
			{
				if (i + size >= resp->size) size = resp->size - i;
				write(fd, resp->memory + i, size);
				i = i + size;
			}
		}

		/* process feed*/
		struct xmlContext ctx = {
			.stage = INIT,
			.attrSize = 0,
			.dataSize = 0
		};
		struct xmlCallBack cb;
		cb.begin = begin;
		cb.end = end;
		cb.content = content;
		parseXML(resp->memory, resp->size, &ctx, &cb);
	} else {
		printf("fail to get rss, url is %s\n", feedURL);
	}

	/* clean */
	if (fd != -1)	close(fd);
	if (resp) {
		if (resp->size)	free(resp->memory);
		free(resp);
	}
	
	return 0;
}
