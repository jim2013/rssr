rssr:	rssr.c
	cc -Os -Wall -Wextra -lcurl -std=c99 -o rssr rssr.c

althttpd:	althttpd.c
	cc -Os -Wall -Wextra -o althttpd althttpd.c

rss_tool: rss_tool.c
	cc -Os -Wall -Wextra -std=c99 -o rss_tool rss_tool.c
test: xml.h json.h xml.c test.c json.c
	cc -Os -Wall -Wextra -std=c99 -o test xml.c test.c json.c

clean:	
	rm -f althttpd rssr test
