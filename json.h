#ifndef __JSON_H__
#define __JSON_H__

#define VAL_OBJ 0
#define VAL_ARR 1
#define VAL_STR 2
/* number type */
#define VAL_INT 3
#define VAL_DOU 4
#define VAL_EXP 5

#define VAL_TRUE 6
#define VAL_FALSE 7
#define VAL_NULL 8

struct jsonContext
{
	int status;
	char **names;
	int *valType;
	char *val;
	int level;
	int size;

	void *clientData;
};

struct jsonCallBack
{
	void (*begin)(char *name, int valType, int level, void *clientData);
	void (*end)(char *name, int valType, int level, void *clientData);
	void (*kv)(char *name, char *val, int valType, int level, void *clientData);
};

int parseJSON(char *data, int size, struct jsonContext *ctx, 
		struct jsonCallBack *cb);

struct jsonContext *newJSONContext(void *clientData);

void freeJSONContext(struct jsonContext *ctx);
#endif
