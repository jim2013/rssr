/* A basic json parser
 *
 *
 * # name convention
 * obj: object
 * arr: array
 * str: string
 * num: number
 * dou: doule
 * exp: exponent
 * ws: white space
 * val: value
 */

#include <stdio.h>
#include <stdlib.h>

#include "json.h"

#define IS_WS(ch) (ch == '\t' || ch == '\n' || ch == '\r' || ch == ' ')
#define IS_DIGIT(ch) (ch >= '0' && ch <= '9')

#define STATUS_VAL -1
#define STATUS_OBJ 0
#define STATUS_ARR 1
#define STATUS_STR 2
#define STATUS_INT 3
#define STATUS_DOU 4
#define STATUS_EXP 5
#define STATUS_TRUE 6
#define STATUS_FALSE 7
#define STATUS_NULL 8

#define STATUS_NAME 20
#define STATUS_COLON 21
#define STATUS_MORE 22
#define STATUS_ARR_MORE 23


#define LEVEL_MAX 8
#define NAME_MAX 64
#define VAL_MAX 1024

#define C_OK 0
#define C_ERR 1

struct jsonContext *newJSONContext(void *clientData)
{
	struct jsonContext *ctx = malloc(sizeof(struct jsonContext));
	ctx->status = STATUS_VAL;
	ctx->names = malloc(LEVEL_MAX * sizeof(char *));
	for (int i = 0; i < LEVEL_MAX; i++)
		ctx->names[i] = malloc((NAME_MAX + 1) * sizeof(char));
	ctx->valType = malloc(LEVEL_MAX * sizeof(int));
	/* \0 terminated */
	ctx->val = malloc((VAL_MAX + 1) * sizeof(char));
	ctx->level = 0;
	ctx->size = 0;
	ctx->clientData = clientData;
	ctx->names[0][0] = '\0';
	return ctx;
}

void freeJSONContext(struct jsonContext *ctx)
{
	if (ctx == NULL)  return;
	if (ctx->names != NULL)
	{
		for (int i = 0; i < LEVEL_MAX; i++)
		{
			if (ctx->names[i] != NULL)  free(ctx->names[i]);
		}
		free(ctx->names);
	}
	if (ctx->valType != NULL)  free(ctx->valType);
	if (ctx->val)  free(ctx->val);
}

int endCallBack(struct jsonContext *ctx, struct jsonCallBack *cb, char ch)
{
	int valType = ctx->valType[ctx->level];
	if (valType == VAL_OBJ)
	{
		if (ch != '}') 
		{
			printf("except }, but found:%c\n", ch);
			return C_ERR;
		}
	} else if (valType == VAL_ARR) {
		if (ch != ']')
		{
			printf("except ], but found:%c\n", ch);
			return C_ERR;
		}
	} else {
		printf("unexcepted end delimiter character:%c\n", ch);
		return C_ERR;
	}

	if (cb && cb->end)
		cb->end(ctx->names[ctx->level], valType, ctx->level, ctx->clientData);

	ctx->level--;
	if (ctx->level > -1)
	{
		if (ctx->valType[ctx->level] == VAL_ARR)
		{
			ctx->status = STATUS_ARR_MORE;
		} else {
			ctx->status = STATUS_MORE;
		}
	}
	return C_OK;
}

void kvCallBack(struct jsonContext *ctx, struct jsonCallBack *cb)
{
	ctx->val[ctx->size] = '\0';
	if (cb && cb->kv)
		cb->kv(ctx->names[ctx->level], ctx->val, ctx->valType[ctx->level], 
				ctx->level, ctx->clientData);

	ctx->level--;
	if (ctx->level > -1)
	{
		if (ctx->valType[ctx->level] == VAL_ARR)
		{
			ctx->status = STATUS_ARR_MORE;
		} else {
			ctx->status = STATUS_MORE;
		}
	}
}


int parseJSON(char *data, int size, struct jsonContext *ctx, 
		struct jsonCallBack *cb)
{
	if (data == NULL || size < 1)
	{
		printf("empty data.\n");
		return 0;
	}
	int i = 0;
	while(i < size)
	{
		char ch = data[i++];
		if (ctx->level < 0 && !IS_WS(ch))
		{
			printf("unexcepted status: there are non ws characters while level little than 0.\n");
			goto err;
		}
		if (ctx->status == STATUS_VAL)
		{
			if (ch == '{')
			{
				ctx->valType[ctx->level] = VAL_OBJ;
				/* call back */
				if (cb && cb->begin)
					cb->begin(ctx->names[ctx->level], VAL_OBJ, ctx->level,
						   	ctx->clientData);

				ctx->status = STATUS_OBJ;
			} else if (ch == '[') {
				ctx->valType[ctx->level] = VAL_ARR;
				if (cb && cb->begin)
					cb->begin(ctx->names[ctx->level], VAL_ARR, ctx->level,
							ctx->clientData);
				ctx->status = STATUS_ARR;
			} else if (ch == '"') {
				ctx->valType[ctx->level] = VAL_STR;
				ctx->status = STATUS_STR;
				ctx->size = 0;
			} else if (ch == 't') {
				ctx->valType[ctx->level] = VAL_TRUE;
				ctx->status = STATUS_TRUE;
				ctx->val[0] = ch;
				ctx->size = 1;
			} else if (ch == 'f') {
				ctx->valType[ctx->level] = VAL_FALSE;
				ctx->status = STATUS_FALSE;
				ctx->val[0] = ch;
				ctx->size = 1;
			} else if (ch == 'n' ) {
				ctx->valType[ctx->level] = VAL_NULL;
				ctx->status = STATUS_NULL;
				ctx->val[0] = ch;
				ctx->size = 1;
			} else if (IS_DIGIT(ch) || ch == '-') {
				ctx->valType[ctx->level] = VAL_INT;
				ctx->status = STATUS_INT;
				ctx->val[0] = ch;
				ctx->size = 1;
			} else if (!IS_WS(ch)) {
				printf("except beginning character of  a value, but found %c\n", ch);
			}
		} else if (ctx->status == STATUS_OBJ) {
			if (ch == '"')
			{
				/* beginning of name */
				if (ctx->level >= LEVEL_MAX - 1)
				{
					printf("exceed LEVEL_MAX.\n");
					goto err;
				}
				ctx->level++;
				ctx->status = STATUS_NAME;
				ctx->size = 0;
			} else if (ch == '}') {
				/* empty object */
				if (endCallBack(ctx, cb, ch) != C_OK)
					goto err;
			} else if (!IS_WS(ch)) {
				printf("name must start with \"\n");
				goto err;
			}
		} else if (ctx->status == STATUS_NAME) {
			if (ctx->size > -1) {
				if (ch == '"') 
				{
					ctx->names[ctx->level][ctx->size] = '\0';
					ctx->status = STATUS_COLON;
				} else {
					if (ctx->size >= NAME_MAX - 1)
					{
						printf("exceed name max.\n");
						goto err;
					}
					ctx->names[ctx->level][ctx->size++] = ch;
				}
			} else {
				/* looking for the begining quote */
				if (ch == '"')
				{
					if (ctx->level >= LEVEL_MAX - 1)
					{
						printf("exceed LEVEL_MAX.\n");
						goto err;
					}
					ctx->level++;
					ctx->size = 0;
				} else if (!IS_WS(ch)) {
					printf("name must start with \"\n");
					goto err;
				}
			}
		} else if (ctx->status == STATUS_COLON) {
			if (ch == ':') {
				ctx->status = STATUS_VAL;
			} else if (!IS_WS(ch)){
				printf("except a colon, but found %c\n", ch);
				goto err;
			}
		} else if (ctx->status == STATUS_STR) {
			if (ch == '"')
			{
				kvCallBack(ctx, cb);
			} else {
				if (ctx->size >= VAL_MAX - 1)
				{
					printf("exceed value max.\n");
					goto err;
				}
				ctx->val[ctx->size++] = ch;
			}
		} else if (ctx->status == STATUS_TRUE || 
					ctx->status == STATUS_FALSE ||
					ctx->status == STATUS_NULL) {
			char *terminal = "true";
			int len = 4;
			if (ctx->status == STATUS_FALSE)
			{
				terminal = "false";
				len = 5;
			} else if (ctx->status == STATUS_NULL) {
				terminal = "null";
				len = 4;
			}

			if (ctx->size >= len)
			{
				printf("unexcepted size in true/false/null status.\n");
				goto err;
			}

			if (terminal[ctx->size] == ch) {
				if (ctx->size > VAL_MAX - 1)
				{
					printf("exceed value max.\n");
					goto err;
				}
				ctx->val[ctx->size++] = ch;
				if (ctx->size == len)
					kvCallBack(ctx, cb);
			} else {
				printf("unknown value.\n");
				goto err;
			}
		} else if (ctx->status == STATUS_INT || ctx->status == STATUS_DOU ||
				ctx->status == STATUS_EXP) {
			/* TODO 000 or 1e000 is not permitted in JSON specification. */
			if (ch == ',' || IS_WS(ch) || ch == '}' || ch == ']')
			{
				kvCallBack(ctx, cb);
				/* take a step back so the character can be processed in more 
				 * or arr_more status*/
				i--;
			} else {
				if (ch == '.') 
				{
					if (ctx->status != STATUS_INT)
					{
						printf("malformed number: unexcepted %c\n", ch);
						goto err;
					}
					ctx->valType[ctx->level] = VAL_DOU;
					ctx->status = STATUS_DOU;
				} else if (ch == 'e' || ch == 'E') {
					if (ctx->status != STATUS_INT && 
							ctx->status != STATUS_DOU)
					{
						printf("malformed number: unexcepted %c\n", ch);
						goto err;
					}
					ctx->valType[ctx->level] = VAL_EXP;
					ctx->status = STATUS_EXP;
				} else if (ch == '-' || ch == '+') {
					if(ctx->size < 2 || 
					(ctx->val[ctx->size - 1] != 'e' && ctx->val[ctx->size - 1] != 'E'))
					{
						printf("malformed number: unexcepted %c\n", ch);
						goto err;
					}
				} else if (!IS_DIGIT(ch)){
					printf("malformed number not digital %c\n", ch);
					goto err;
				}

				if (ctx->size >= VAL_MAX - 1)
				{
					printf("exceed value max.\n");
					goto err;
				}
				ctx->val[ctx->size++] = ch;
			}
		} else if (ctx->status == STATUS_MORE) {
			if (ch == ',') {
				ctx->status = STATUS_NAME;
				ctx->size = -1;
			} else if (ch == '}') {
				if (endCallBack(ctx, cb, ch) != C_OK)
					goto err;
			} else if (!IS_WS(ch)){
				printf("except more or end, but found %c\n", ch);
				goto err;
			}
		} else if (ctx->status == STATUS_ARR) {
			if (ch == ']') 
			{
				/* empty array */
				if (endCallBack(ctx, cb, ch) != C_OK)
					goto err;
			} else if (!IS_WS(ch)) {
				/* not empty array, take a step back so that the character will 
				 * be processed in STATUS_VAL case */
				i--;
				ctx->status = STATUS_VAL;
				/* array just contain values, so we set the name to empty */
				if (ctx->level >= LEVEL_MAX - 1)
				{
					printf("exceed level max.\n");
					goto err;
				}
				ctx->names[++ctx->level][0] = '\0';
			}
		} else if (ctx->status == STATUS_ARR_MORE) {
			if (ch == ',') 
			{
				ctx->status = STATUS_VAL;
				/* array just contain values, so we set the name to empty */
				if (ctx->level >= LEVEL_MAX - 1)
				{
					printf("exceed level max.\n");
					goto err;
				}
				ctx->names[++ctx->level][0] = '\0';
			} else if (ch == ']') {
				if (endCallBack(ctx, cb, ch) != C_OK)
					goto err;
			}
		}
	}

	return 0;

err:
	printf("error occur, near:");
	for (int k = i - 10 > 0 ? i-10 : 0; k < i+10 && k < size; k++)
		printf("%c", data[k]);

	return 1;
}
